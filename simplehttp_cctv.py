#!/usr/bin/env python
'''
	Author: Igor Maculan - n3wtron@gmail.com
	A Simple mjpg stream http server
'''

from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
import StringIO
import time
import cv2, numpy, datetime
from PIL import Image, ImageDraw , ImageFont
import os.path,sys,django
for p in sys.path: print (p)
sys.path.append("/home/ubuntu/embeded_python/embedded_cctv")
os.environ["DJANGO_SETTINGS_MODULE"] = "embedded.settings"
django.setup()

from cctv.models import Capture,AlarmTime
capture=None


class CamHandler(BaseHTTPRequestHandler):
	
	def do_GET(self):
		if self.path.endswith('.mjpg'):
			self.send_response(200)
			self.send_header('Content-type','multipart/x-mixed-replace; boundary=--jpgboundary')
			self.end_headers()
			M_counter = 0
			U_counter = 0
			while True:
				try:
					rc,img = capture.read()
					if not rc:
						continue
					imgRGB=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
					jpg = Image.fromarray(imgRGB)
					pil_img = Image.fromarray( img ) # create PIL image from numpy array
					draw = ImageDraw.Draw( pil_img )
					now = datetime.datetime.now() # get current date & time
					timestamp = now.strftime( "Date: %Y-%m-%d, Time: %H:%M:%S" )
					Font = ImageFont.truetype("/usr/share/fonts/truetype/freefont/FreeSansBold.ttf", 18)
					del draw
					cv2.imshow('VideoCapture', numpy.array( pil_img ) )
					key = cv2.waitKey(1)
					if key == ord('s'): # press Spacebar to save file
						draw = ImageDraw.Draw( pil_img )
						draw.text( (10, 450),timestamp+'   MotionSensor Captured', fill='#80ff80' ,font = Font )
						PIC = cv2.imwrite( '/home/notham/Desktop/embedded_cctv/cctv/static/'+'Mcapture'+str(M_counter)+str(timestamp)+'.jpg', numpy.array(pil_img) )
						Capture.objects.create(filename = 'Mcapture'+str(M_counter)+'.jpg')
						M_counter +=1
					if key == ord('a'):
						draw = ImageDraw.Draw( pil_img )
						draw.text( (10, 450),timestamp+'   User Capture', fill='#80ff80' ,font = Font )
						PIC = cv2.imwrite( '/home/notham/Desktop/embedded_cctv/cctv/static/'+'Ucapture'+str(U_counter)+str(timestamp)+'.jpg', numpy.array(pil_img) )
						Capture.objects.create(filename = 'Ucapture'+str(U_counter)+'.jpg')
						print(U_counter)
						U_counter +=1
						
					tmpFile = StringIO.StringIO()
					jpg.save(tmpFile,'JPEG')
					self.wfile.write("--jpgboundary")
					self.send_header('Content-type','image/jpeg')
					self.send_header('Content-length',str(tmpFile.len))
					self.end_headers()
					jpg.save(self.wfile,'JPEG')
					time.sleep(0.05)
				except KeyboardInterrupt:
					break
			return
		if self.path.endswith('.html'):
			self.send_response(200)
			self.send_header('Content-type','text/html')
			self.end_headers()
			self.wfile.write('<html><head></head><body>')
			self.wfile.write('<img src="http://127.0.0.1:8080/cam.mjpg"/>')
			self.wfile.write('</body></html>')
			return

def main():
	global capture
	capture = cv2.VideoCapture(0)
	capture.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, 640); 
	capture.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, 480);
	capture.set(cv2.cv.CV_CAP_PROP_SATURATION,0.75);
	global img
	try:
		server = HTTPServer(('',8080),CamHandler)
		print ("server started")
		server.serve_forever()
	except KeyboardInterrupt:
		capture.release()
		server.socket.close()

if __name__ == '__main__':

	main()
