from django.conf.urls import url

from cctv import views

urlpatterns = [
    url(r'^$', views.home_page, name='home'),
    url(r'^alert/$', views.alert, name='alert'),
    url(r'^stop_alert/$', views.stop_alert, name='stop_alert'),
    url(r'^turn_left/$', views.turn_left, name='turn_left'),
    url(r'^turn_right/$', views.turn_right, name='turn_right'),
    url(r'^capture/(.+)/$', views.capture, name='capture'),
    # url(r'^admin/', include(admin.site.urls)),
]
