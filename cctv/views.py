from django.http import HttpResponse
from django.shortcuts import render, redirect
from cctv.models import Capture, AlarmTime
import socket
showtext =""

def home_page(request):
    if request.method == 'POST':
        if request.POST.get('send_capture', '') == 'send_capture':
            socket_sent(b'capture')
        if request.POST.get('send_turnleft', '') == 'send_turnleft':
            socket_sent(b'turn left')
        if request.POST.get('send_turnright', '') == 'send_turnright':
            socket_sent(b'turn right')
        if request.POST.get('send_alarm', '') == 'send_alarm':
            if request.POST['day'] != '' and request.POST['starttime_hour'] != '' and request.POST['starttime_min'] != '' and request.POST['stoptime_hour'] != '' and request.POST['stoptime_min'] != '':
                isSet = AlarmTime.objects.filter(
                            weekday=request.POST['day'],
                            start_hour = request.POST['starttime_hour'],
                            start_min = request.POST['starttime_min'],
                            stop_hour = request.POST['stoptime_hour'],
                            stop_min = request.POST['stoptime_min'],
                         ).exists()
                #print (isSet)
                if not isSet :
                    if int(request.POST['starttime_hour'])>int(request.POST['stoptime_hour']):
                        # แสดงข้อความ เลือกเวลา start มากกว่าเวลา stop
                        print('start>stop')
                    elif int(request.POST['starttime_hour'])==int(request.POST['stoptime_hour']) and int(request.POST['starttime_min'])>int(request.POST['stoptime_min']):
                        print('start>stop')
                    else:
                        alarmtime_create = AlarmTime.objects.create(
                            weekday = request.POST.get('day', ''),
                            start_hour = request.POST.get('starttime_hour', ''),
                            start_min = request.POST.get('starttime_min', ''),
                            stop_hour = request.POST.get('stoptime_hour', ''),
                            stop_min = request.POST.get('stoptime_min', ''),
                        )
                        socket_sent(b'alarm_set')

        if request.POST.get('delete', '') == 'delete':
            id_delete = request.POST['id_delete']
            AlarmTime.objects.get(pk=id_delete).delete()
            socket_sent(b'alarm set')

    alarmtime_s = AlarmTime.objects.filter(weekday='Sunday').order_by('start_hour','start_min','stop_hour','stop_min')
    alarmtime_m = AlarmTime.objects.filter(weekday='Monday').order_by('start_hour','start_min','stop_hour','stop_min')
    alarmtime_t = AlarmTime.objects.filter(weekday='Tuesday').order_by('start_hour','start_min','stop_hour','stop_min')
    alarmtime_w = AlarmTime.objects.filter(weekday='Wednesday').order_by('start_hour','start_min','stop_hour','stop_min')
    alarmtime_th = AlarmTime.objects.filter(weekday='Thursday').order_by('start_hour','start_min','stop_hour','stop_min')
    alarmtime_f = AlarmTime.objects.filter(weekday='Friday').order_by('start_hour','start_min','stop_hour','stop_min')
    alarmtime_sa = AlarmTime.objects.filter(weekday='Saturday').order_by('start_hour','start_min','stop_hour','stop_min')

    capture_ = Capture.objects.all().order_by('id')
    return render(request, 'home.html', {
        #'day': request.POST.get('day', ''),
        #'starttime': request.POST.get('starttime_hour', '')+":"+request.POST.get('starttime_min', ''),
        #'stoptime': request.POST.get('stoptime_hour', '')+":"+request.POST.get('stoptime_min', ''),
        'captures' : capture_,
        'hour_range' : range(0, 24),
        'min_range' : range(0, 60),
        'alarmtimes_s' : alarmtime_s,
        'alarmtimes_m' : alarmtime_m,
        'alarmtimes_t' : alarmtime_t,
        'alarmtimes_w' : alarmtime_w,
        'alarmtimes_th' : alarmtime_th,
        'alarmtimes_f' : alarmtime_f,
        'alarmtimes_sa' : alarmtime_sa,
        'showtext' : showtext, 
    })


def socket_sent (MESSAGE):
    TCP_IP = '127.0.0.1'
    TCP_PORT = 5000


def alert (request):
    global showtext 
    showtext = "Alerting!!!"
    return redirect('/')

def stop_alert (request):
    global showtext 
    showtext = ""
    return redirect('/')


def turn_left (request):
    socket_sent(b'turn left')
    return redirect('/')

def turn_right (request):
    socket_sent(b'turn right')
    return redirect('/')

def capture(request, filename_):
    Capture.objects.create(filename = str(filename_))
    print(filename_)
    socket_sent(b'capture')
    return redirect('/')

def socket_sent (MESSAGE):

    TCP_IP = '127.0.0.1'
    TCP_PORT = 5000

    BUFFER_SIZE = 1024
 
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    s.connect((TCP_IP, TCP_PORT))
    s.send(MESSAGE)
    #data = s.recv(BUFFER_SIZE)
    s.close()
    
   # print ("received data:", data)
