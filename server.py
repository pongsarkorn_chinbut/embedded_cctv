import urllib
import urllib.request as urllib2
import socket, select
import os.path,sys,django
import time
import serial
sys.path.append("home/ubuntu/embeded_python/embedded_cctv")
os.environ["DJANGO_SETTINGS_MODULE"] = "embedded.settings"
django.setup()

from cctv.models import Capture,AlarmTime

port_out='/dev/ttyACM1'

if __name__ == "__main__":
      
    CONNECTION_LIST = []    # list of socket clients
    RECV_BUFFER = 4096 # Advisable to keep it as an exponent of 2
    PORT = 5000
         
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # this has no effect, why ?
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(("0.0.0.0", PORT))
    server_socket.listen(10)
 
    # Add server socket to the list of readable connections
    CONNECTION_LIST.append(server_socket)
 

    while 1:
        # Get the list sockets which are ready to be read through select
        read_sockets,write_sockets,error_sockets = select.select(CONNECTION_LIST,[],[])
 
        for sock in read_sockets:
             
            #New connection
            if sock == server_socket:
                # Handle the case in which there is a new connection recieved through server_socket
                sockfd, addr = server_socket.accept()
                CONNECTION_LIST.append(sockfd)
                print ("Client (%s, %s) connected" % addr)
                 
            #Some incoming message from a client
            else:
                # Data recieved from client, process it
                try:
                    #In Windows, sometimes when a TCP program closes abruptly,
                    # a "Connection reset by peer" exception will be thrown
                    data = sock.recv(RECV_BUFFER)
                    # echo back the client message
                    print(data)
                    if data:
                        ser = serial.Serial(
                               port=port_out,
                               baudrate = 115200,
                               parity=serial.PARITY_NONE,
                               stopbits=serial.STOPBITS_ONE,
                               bytesize=serial.EIGHTBITS,
                               timeout=1
                           )
                        data_in=data.decode('utf-8')
                        print(data_in)

                        if(data_in=="alarm_set"):
                           
                           for p in AlarmTime.objects.filter(weekday='Monday').values():
                               print (p)

                           message = "time"
                           ser.write(bytes(message.encode('ascii')))
                        
                        if(data_in=="turn left"):
                           message = "left"
                           ser.write(bytes(message.encode('ascii')))

                        if(data_in=="turn right"):
                           message = "right"
                           ser.write(bytes(message.encode('ascii')))
  
                        if(data_in=="capture"):
                           message = "capture"
                           ser.write(bytes(message.encode('ascii')))

                        if(data_in=="Alert"):
                           req = urllib2.Request('http://127.0.0.1:8000/alert')
                           response = urllib2.urlopen(req)

                        if(data_in=="stop_Alert"):
                           req = urllib2.Request('http://127.0.0.1:8000/stop_alert')
                           response = urllib2.urlopen(req)

                    sock.close()
                    CONNECTION_LIST.remove(sock)
                # client disconnected, so remove from socket list
                except:
                    print ("Client (%s, %s) is offline" % addr)
                    sock.close()
                    CONNECTION_LIST.remove(sock)
                    continue
         
    server_socket.close()
