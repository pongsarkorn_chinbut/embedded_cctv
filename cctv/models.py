from django.db import models

class Capture(models.Model):
    filename = models.TextField(default='')

class AlarmTime(models.Model):
    weekday = models.TextField(default='Sunday')
    start_hour = models.IntegerField(default=0)
    start_min = models.IntegerField(default=0)
    stop_hour = models.IntegerField(default=0)
    stop_min = models.IntegerField(default=0)
