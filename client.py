#!/usr/bin/env python
import time
import serial
import socket
import urllib
import urllib2
import sys

ser = serial.Serial(
   port='/dev/ttyACM1',
   baudrate = 115200,
   parity=serial.PARITY_NONE,
   stopbits=serial.STOPBITS_ONE,
   bytesize=serial.EIGHTBITS,
   timeout=1
   )

counter=0
          
while 1:
   x=ser.readline()
   if(x == "a"):
      # Create a TCP/IP socket
      sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      # Connect the socket to the port where the server is listening
      server_address = ('localhost', 5000)
      sock.connect(server_address)
      sock.sendall('Alert')
      sock.close()
   print x
