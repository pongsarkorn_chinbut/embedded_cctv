# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cctv', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AlarmTime',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('weekday', models.TextField(default='Sunday')),
                ('start_hour', models.IntegerField(default=0)),
                ('start_min', models.IntegerField(default=0)),
                ('stop_hour', models.IntegerField(default=0)),
                ('stop_min', models.IntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
